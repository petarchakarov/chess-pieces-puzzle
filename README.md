This program places a user defined number of Kings, Queens, Bishops, Rooks and Knights on a M * N chess board in such a way that no two pieces threaten each other. The color of the piece doesn't matter.

It can solve the Eight queens puzzle and different variations of it.