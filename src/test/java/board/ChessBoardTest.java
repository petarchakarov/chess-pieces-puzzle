package board;

import static org.junit.Assert.*;
import org.junit.Test;
import board.ChessBoard;

/**
 * Unit tests for ChessBoard class.
 * 
 * @author Petar Chakarov
 * @see ChessBoard
 *
 */
public class ChessBoardTest {
    /**
     * Tests various isFree* methods.
     */
    @Test
    public void testIsFreeMethods() {
        ChessBoard board = new ChessBoard(8, 8);
        board.placePieceOnPosition(Piece.BISHOP.value());
        board.next();
        
        assertEquals(false, board.isPositionFree(0, 0)); // not free
        assertEquals(true, board.isPositionFree(-1, 0)); // free because outside of board
        assertEquals(true, board.isPositionFree(0, 10)); // free because outside of board
        assertEquals(false, board.isRowFree(0)); // not free
        assertEquals(true, board.isRowFree(1)); // should be free
        assertEquals(false, board.isColFree(0)); // not free
        assertEquals(true, board.isColFree(1)); // should be free
        assertEquals(false, board.areDiagonalsFree(3, 3)); // not free
        assertEquals(true, board.areDiagonalsFree(0, 1)); // should be free
    }
    /**
     * Tests setPositionUnderAttack method.
     */
    @Test
    public void testSetPositionUnderAttack() {
        ChessBoard board = new ChessBoard(8, 8);
        board.setPositionUnderAttack(0, 1);
        board.next();
        
        assertEquals(0, board.getCurrentRow());
        assertEquals(2, board.getCurrentCol());
    }
    /**
     * Tests setRowUnderAttack method.
     */
    @Test
    public void testSetRowUnderAttack() {
        ChessBoard board = new ChessBoard(8, 8);
        board.setRowUnderAttack(0);
        board.next();
        
        assertEquals(1, board.getCurrentRow());
        assertEquals(0, board.getCurrentCol());
    }
    /**
     * Tests setColUnderAttack method.
     */
    @Test
    public void testSetColUnderAttack() {
        ChessBoard board = new ChessBoard(8, 8);
        board.setColUnderAttack(0);
        board.setColUnderAttack(1);
        board.next();
        
        assertEquals(0, board.getCurrentRow());
        assertEquals(2, board.getCurrentCol());
    }
    /**
     * Tests setDiagonalsUnderAttack method.
     */
    @Test
    public void testSetDiagonalsUnderAttack() {
        ChessBoard board = new ChessBoard(8, 8);
        board.setColUnderAttack(0);
        board.setRowUnderAttack(0);
        board.setDiagonalsUnderAttack(3, 3);
        board.next();
        
        assertEquals(1, board.getCurrentRow());
        assertEquals(2, board.getCurrentCol());
    }
}
