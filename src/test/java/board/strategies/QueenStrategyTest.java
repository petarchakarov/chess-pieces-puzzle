package board.strategies;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import board.ChessBoard;
import board.Piece;

/**
 * Unit tests for QueenStrategy class.
 * 
 * @author Petar Chakarov
 * @see QueenStrategy
 *
 */
public class QueenStrategyTest {
    /**
     * Tests placing a queen when positions are not free.
     */
    @Test
    public void testPlaceNotPossible() {
        final int row = 3;
        final int col = 4;
        
        ChessBoard board = mock(ChessBoard.class);
        when(board.getCurrentRow()).thenReturn(row);
        when(board.getCurrentCol()).thenReturn(col);
        when(board.isRowFree(row)).thenReturn(true);
        when(board.isColFree(col)).thenReturn(false);
        when(board.areDiagonalsFree(row, col)).thenReturn(true);
        
        QueenStrategy strategy = new QueenStrategy();
        boolean placed = strategy.place(board);
        
        assertEquals(false, placed);
    }
    /**
     * Tests placing a queen when positions are free.
     */
    @Test
    public void testPlace() {
        final int row = 3;
        final int col = 4;
        
        ChessBoard board = mock(ChessBoard.class);
        when(board.getCurrentRow()).thenReturn(row);
        when(board.getCurrentCol()).thenReturn(col);
        when(board.isRowFree(row)).thenReturn(true);
        when(board.isColFree(col)).thenReturn(true);
        when(board.areDiagonalsFree(row, col)).thenReturn(true);
        
        QueenStrategy strategy = new QueenStrategy();
        boolean placed = strategy.place(board);
        
        verify(board).setRowUnderAttack(row);
        verify(board).setColUnderAttack(col);
        verify(board).setDiagonalsUnderAttack(row, col);
        verify(board).placePieceOnPosition(Piece.QUEEN.value());
        
        assertEquals(true, placed);
    }
}
