package board.strategies;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import board.ChessBoard;
import board.Piece;

/**
 * Unit tests for KnightStrategy class.
 * 
 * @author Petar Chakarov
 * @see KnightStrategy
 *
 */
public class KnightStrategyTest {
    /**
     * Tests placing a knight when positions are not free.
     */
    @Test
    public void testPlaceNotPossible() {
        final int row = 3;
        final int col = 4;
        
        ChessBoard board = mock(ChessBoard.class);
        when(board.getCurrentRow()).thenReturn(row);
        when(board.getCurrentCol()).thenReturn(col);
        when(board.isPositionFree(row - 1, col - 2)).thenReturn(true);
        when(board.isPositionFree(row + 1, col - 2)).thenReturn(true);
        when(board.isPositionFree(row + 2, col - 1)).thenReturn(true);
        when(board.isPositionFree(row + 2, col + 1)).thenReturn(true);
        when(board.isPositionFree(row - 1, col + 2)).thenReturn(true);
        when(board.isPositionFree(row + 1, col + 2)).thenReturn(false);
        when(board.isPositionFree(row - 2, col - 1)).thenReturn(true);
        when(board.isPositionFree(row - 2, col + 1)).thenReturn(true);
        
        KnightStrategy strategy = new KnightStrategy();
        boolean placed = strategy.place(board);
        
        assertEquals(false, placed);
    }
    /**
     * Tests placing a knight when positions are free.
     */
    @Test
    public void testPlace() {
        final int row = 3;
        final int col = 4;
        
        ChessBoard board = mock(ChessBoard.class);
        when(board.getCurrentRow()).thenReturn(row);
        when(board.getCurrentCol()).thenReturn(col);
        when(board.isPositionFree(row - 1, col - 2)).thenReturn(true);
        when(board.isPositionFree(row + 1, col - 2)).thenReturn(true);
        when(board.isPositionFree(row + 2, col - 1)).thenReturn(true);
        when(board.isPositionFree(row + 2, col + 1)).thenReturn(true);
        when(board.isPositionFree(row - 1, col + 2)).thenReturn(true);
        when(board.isPositionFree(row + 1, col + 2)).thenReturn(true);
        when(board.isPositionFree(row - 2, col - 1)).thenReturn(true);
        when(board.isPositionFree(row - 2, col + 1)).thenReturn(true);
        
        KnightStrategy strategy = new KnightStrategy();
        boolean placed = strategy.place(board);
        
        verify(board).setPositionUnderAttack(row - 1, col - 2);
        verify(board).setPositionUnderAttack(row + 1, col - 2);
        verify(board).setPositionUnderAttack(row + 2, col - 1);
        verify(board).setPositionUnderAttack(row + 2, col + 1);
        verify(board).setPositionUnderAttack(row - 1, col + 2);
        verify(board).setPositionUnderAttack(row + 1, col + 2);
        verify(board).setPositionUnderAttack(row - 2, col - 1);
        verify(board).setPositionUnderAttack(row - 2, col + 1);
        verify(board).placePieceOnPosition(Piece.KNIGHT.value());
        assertEquals(true, placed);
    }
}
