package board.strategies;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import board.ChessBoard;
import board.Piece;

/**
 * Unit tests for KingStrategy class.
 * 
 * @author Petar Chakarov
 * @see KingStrategy
 *
 */
public class KingStrategyTest {
    /**
     * Tests placing a king when neighboring positions are not free.
     */
    @Test
    public void testPlaceNotPossible() {
        final int row = 3;
        final int col = 4;
        
        ChessBoard board = mock(ChessBoard.class);
        when(board.getCurrentRow()).thenReturn(row);
        when(board.getCurrentCol()).thenReturn(col);
        when(board.isPositionFree(row, col + 1)).thenReturn(true);
        when(board.isPositionFree(row, col - 1)).thenReturn(true);
        when(board.isPositionFree(row + 1, col)).thenReturn(true);
        when(board.isPositionFree(row, col)).thenReturn(true);
        when(board.isPositionFree(row + 1, col - 1)).thenReturn(true);
        when(board.isPositionFree(row - 1, col)).thenReturn(false);
        when(board.isPositionFree(row - 1, col + 1)).thenReturn(true);
        when(board.isPositionFree(row - 1, col - 1)).thenReturn(true);
        
        KingStrategy strategy = new KingStrategy();
        boolean placed = strategy.place(board);
        
        assertEquals(false, placed);
    }
    /**
     * Tests placing a king when neighboring positions are free.
     */
    @Test
    public void testPlace() {
        final int row = 3;
        final int col = 4;
        
        ChessBoard board = mock(ChessBoard.class);
        when(board.getCurrentRow()).thenReturn(row);
        when(board.getCurrentCol()).thenReturn(col);
        when(board.isPositionFree(row, col + 1)).thenReturn(true);
        when(board.isPositionFree(row, col - 1)).thenReturn(true);
        when(board.isPositionFree(row + 1, col)).thenReturn(true);
        when(board.isPositionFree(row + 1, col + 1)).thenReturn(true);
        when(board.isPositionFree(row + 1, col - 1)).thenReturn(true);
        when(board.isPositionFree(row - 1, col)).thenReturn(true);
        when(board.isPositionFree(row - 1, col + 1)).thenReturn(true);
        when(board.isPositionFree(row - 1, col - 1)).thenReturn(true);
        
        KingStrategy strategy = new KingStrategy();
        boolean placed = strategy.place(board);
        
        verify(board).setPositionUnderAttack(row, col + 1);
        verify(board).setPositionUnderAttack(row, col - 1);
        verify(board).setPositionUnderAttack(row + 1, col);
        verify(board).setPositionUnderAttack(row + 1, col + 1);
        verify(board).setPositionUnderAttack(row + 1, col - 1);
        verify(board).setPositionUnderAttack(row - 1, col);
        verify(board).setPositionUnderAttack(row - 1, col + 1);
        verify(board).setPositionUnderAttack(row - 1, col - 1);
        verify(board).placePieceOnPosition(Piece.KING.value());
        assertEquals(true, placed);
    }
}
