package board.strategies;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import board.ChessBoard;
import board.Piece;

/**
 * Unit tests for BishopStrategy class.
 * 
 * @author Petar Chakarov
 * @see BishopStrategy
 *
 */
public class BishopStrategyTest {
	/**
	 * Tests placing a bishop when the diagonals are not free.
	 */
	@Test
	public void testPlaceNotPossible() {
	    final int row = 3;
        final int col = 4;
        
		ChessBoard board = mock(ChessBoard.class);
		when(board.getCurrentRow()).thenReturn(row);
		when(board.getCurrentCol()).thenReturn(col);
		when(board.areDiagonalsFree(row, col)).thenReturn(false);
		
		BishopStrategy strategy = new BishopStrategy();
		boolean placed = strategy.place(board);
		
		assertEquals(false, placed);
	}
	/**
     * Tests placing a bishop when the diagonals are free.
     */
    @Test
    public void testPlace() {
        final int row = 3;
        final int col = 4;
        
        ChessBoard board = mock(ChessBoard.class);
        when(board.getCurrentRow()).thenReturn(row);
        when(board.getCurrentCol()).thenReturn(col);
        when(board.areDiagonalsFree(row, col)).thenReturn(true);
        
        BishopStrategy strategy = new BishopStrategy();
        boolean placed = strategy.place(board);
        
        verify(board).setDiagonalsUnderAttack(row, col);
        verify(board).placePieceOnPosition(Piece.BISHOP.value());
        assertEquals(true, placed);
    }
}
