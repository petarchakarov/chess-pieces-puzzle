package main;

import board.ChessBoard;
import board.strategies.AttackStrategy;
import board.strategies.BishopStrategy;
import board.strategies.KingStrategy;
import board.strategies.KnightStrategy;
import board.strategies.QueenStrategy;
import board.strategies.RookStrategy;
import board.Piece;

import java.util.HashMap;
import java.util.Map;

/**
 * ChessFinder class implements an algorithm for searching all possible configurations of placing
 * the provided pieces on a board while no piece threatens each other.
 * 
 * @author Petar Chakarov
 *
 */
public class ChessFinder {
    private AttackStrategy strategy;
    private Map<Piece, AttackStrategy> strategies = new HashMap<Piece, AttackStrategy>();
    private int rows;
    private int cols;
    private Piece[] pieces;
    private int counter = 0;
    private HashMap<String, Boolean> permutations = new HashMap<String, Boolean>();
    /**
     * Constructs a ChessFinder for a board with dimensions rows * columns and the provided pieces.
     * 
     * @param rows  the number of rows
     * @param cols  the number of columns
     * @param pieces    pieces to try to place on the board
     */
    public ChessFinder(int rows, int cols, Piece[] pieces) {
        this.rows = rows;
        this.cols = cols;
        strategies.put(Piece.BISHOP, new BishopStrategy());
        strategies.put(Piece.KING, new KingStrategy());
        strategies.put(Piece.KNIGHT, new KnightStrategy());
        strategies.put(Piece.QUEEN, new QueenStrategy());
        strategies.put(Piece.ROOK, new RookStrategy());
        this.pieces = pieces;
    }
    /**
     * Adds the piece on pieceIndex on the current board.
     * Checks all available positions for the piece and places it on the first one available. 
     * Then tries to place the next piece on the board until all pieces are placed or no available position has left.
     * 
     * @param board board to place the piece on
     * @param pieceIndex    index of the piece to be placed in the pieces array
     */
    private void addPiece(ChessBoard board, int pieceIndex) {
        boolean flag = false;
        // try all positions as a starting point
        for(int i = 0; i < rows * cols; i++) {
            int k = i;
            ChessBoard newBoard = new ChessBoard(board);
            flag = true;
            // advance the board's current pointer to the desired position
            while(k > 0) {
                flag = newBoard.next();
                k--;
            }
            // if there is no available position here, stop
            if(flag == false) {
                return;
            }
            strategy = strategies.get(pieces[pieceIndex]);
            if(strategy.place(newBoard)) {
                if(pieceIndex + 1 == pieces.length) {
                    // all pieces are placed on the board, print it and continue searching for more configurations
                    counter++;
                    System.out.println(newBoard);
                    continue;
                }
                if(newBoard.next() == false) {
                    continue;
                }
                addPiece(new ChessBoard(newBoard), pieceIndex + 1);
            }
        }
    }
    /**
     * Starts the configuration search process.
     */
    public void start() {
        permute(pieces.length);
        System.out.println("Total count: " + counter);
    }
    /**
     * Heap's algorithm provides all permutations for the pieces.
     * 
     * @param n internal algorithm counter
     */
    private void permute(int n) {
        if(n == 1) {
            String key = "";
            for(Piece piece : pieces) {
                key += piece;
            }
            if(permutations.get(key) != null) {
                return;
            }
            permutations.put(key, true);
            addPiece(new ChessBoard(rows, cols), 0);
        } else {
            for(int i = 0; i < n; i++) {
                permute(n - 1);
                int j = 0;
                if(n % 2 == 0) {
                    j = i;
                }
                Piece tmp = pieces[j];
                pieces[j] = pieces[n - 1];
                pieces[n - 1] = tmp;
            }
        }
    }
}
