package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import board.Piece;

/**
 * Entry point for the console application.
 * Inputs the required board dimensions and the number of different pieces and starts the configuration search algorithm.
 * 
 * @author Petar Chakarov
 *
 */
public class Main {
    public static void main(String[] args) {
        int m;
        int n;
        int piecesCount;
        int i;
        List<Piece> pieces = new ArrayList<Piece>();
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter m: ");
        m = sc.nextInt();
        
        System.out.println("Enter n: ");
        n = sc.nextInt();
        
        System.out.println("Enter number of kings: ");
        piecesCount = sc.nextInt();
        for(i = 0; i < piecesCount; i++) {
            pieces.add(Piece.KING);
        }
        
        System.out.println("Enter number of queens: ");
        piecesCount = sc.nextInt();
        for(i = 0; i < piecesCount; i++) {
            pieces.add(Piece.QUEEN);
        }
        
        System.out.println("Enter number of bishops: ");
        piecesCount = sc.nextInt();
        for(i = 0; i < piecesCount; i++) {
            pieces.add(Piece.BISHOP);
        }
        
        System.out.println("Enter number of rooks: ");
        piecesCount = sc.nextInt();
        for(i = 0; i < piecesCount; i++) {
            pieces.add(Piece.ROOK);
        }
        
        System.out.println("Enter number of knights: ");
        piecesCount = sc.nextInt();
        for(i = 0; i < piecesCount; i++) {
            pieces.add(Piece.KNIGHT);
        }
        sc.close();

        ChessFinder finder = new ChessFinder(m, n, pieces.toArray(new Piece[pieces.size()]));
        long startTime = System.currentTimeMillis();
        finder.start();
        long endTime = System.currentTimeMillis();
        System.out.println( "Time: " + (endTime - startTime) / 1000  + " seconds" );
    }
}