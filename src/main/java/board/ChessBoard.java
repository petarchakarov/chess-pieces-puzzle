package board;

/**
 * ChessBoard class represents a chess board with pieces on it.
 * Keeps an internal current position and provides methods for advancing to the next available position.
 * Provides methods for placing a chess piece on a position and marking other positions as under attack.
 * 
 * @author Petar Chakarov
 *
 */
public class ChessBoard {
    private int[][] board;
    private int currentRow;
    private int currentCol;
    /**
     * Marker for a free position (no piece placed on it).
     */
    public static int POSITION_FREE = 0;
    /**
     * Marker for a position under attack from another piece.
     */
    public static int POSITION_BUSY = 1;
    /**
     * Constructs a chess board with rows * cols size.
     * 
     * @param rows  number of rows
     * @param cols  number of columns
     */
    public ChessBoard(int rows, int cols) {
        board = new int[rows][cols];
    }
    /**
     * Constructs a copy of the provided board.
     * 
     * @param oldBoard  the board to be copied
     */
    public ChessBoard(ChessBoard oldBoard) {
        board = new int[oldBoard.board.length][];
        for(int i = 0; i < oldBoard.board.length; i++) {
            board[i] = new int[oldBoard.board[i].length];
            System.arraycopy(oldBoard.board[i], 0, board[i], 0, oldBoard.board[i].length);
        }
        currentRow = oldBoard.currentRow;
        currentCol = oldBoard.currentCol;
    }
    /**
     * Returns the current row on the chess board.
     * 
     * @return  the current row index
     */
    public int getCurrentRow() {
        return currentRow;
    }
    /**
     * Returns the current column on the chess board.
     * 
     * @return  the current column index
     */
    public int getCurrentCol() {
        return currentCol;
    }
    /**
     * Places a piece on the current chess board position.
     * 
     * @param   id  numeric value of the piece (see {@link Piece})
     */
    public void placePieceOnPosition(int id) {
        board[currentRow][currentCol] = id;
    }
    /**
     * Sets the provided position under attack.
     * Does nothing if the position is outside the board.
     * 
     * @param   row     row of the position
     * @param   col     column of the position
     */
    public void setPositionUnderAttack(int row, int col) {
        if(row < 0 || col < 0 || row >= board.length || col >= board[row].length) {
            return;
        }
        board[row][col] = POSITION_BUSY;
    }
    /**
     * Checks if the provided position is free.
     * Returns true if position is outside the box.
     * 
     * @param   row     row of the position
     * @param   col     column of the position
     * @return          <code>true</code> if the position is free
     *                  <code>false</code> otherwise
     */
    public boolean isPositionFree(int row, int col) {
        if(row < 0 || col < 0 || row >= board.length || col >= board[row].length) {
            return true;
        }
        if(board[row][col] == POSITION_FREE || board[row][col] == POSITION_BUSY) {
            return true;
        }
        return false;
    }
    /**
     * Checks if all position on the provided row are free.
     * 
     * @param   row     the row to be checked
     * @return          <code>true</code> if the row is free
     *                  <code>false</code> otherwise
     */
    public boolean isRowFree(int row) {
        for(int i = 0; i < board[row].length; i++) {
            if(board[row][i] != POSITION_FREE && board[row][i] != POSITION_BUSY) {
                return false;
            }
        }
        return true;
    }
    /**
     * Checks if all position on the provided column are free.
     * 
     * @param   col     the column to be checked
     * @return          <code>true</code> if the column is free
     *                  <code>false</code> otherwise
     */
    public boolean isColFree(int col) {
        for(int i = 0; i < board.length; i++) {
            if(board[i][col] != POSITION_FREE && board[i][col] != POSITION_BUSY) {
                return false;
            }
        }
        return true;
    }
    /**
     * Sets all positions on the provided row under attack.
     * 
     * @param   row     the row to be set under attack
     */
    public void setRowUnderAttack(int row) {
        for(int i = 0; i < board[row].length; i++) {
            board[row][i] = POSITION_BUSY;
        }
    }
    /**
     * Sets all positions on the provided column under attack.
     * 
     * @param   col     the column to be set under attack
     */
    public void setColUnderAttack(int col) {
        for(int i = 0; i < board.length; i++) {
            board[i][col] = POSITION_BUSY;
        }
    }
    /**
     * Checks if all diagonals from the provided position are free.
     * 
     * @param   row     row of the position
     * @param   col     column of the position
     * @return          <code>true</code> if all diagonals are free
     *                  <code>false</code> otherwise
     */
    public boolean areDiagonalsFree(int row, int col) {
        int tmpRow;
        int tmpCol;
        // checks main diagonal first half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow >=0 && tmpCol >= 0) {
            if(board[tmpRow][tmpCol] != POSITION_FREE && board[tmpRow][tmpCol] != POSITION_BUSY) {
                return false;
            }
            tmpRow -= 1;
            tmpCol -= 1;
        }
        // checks main diagonal second half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow < board.length && tmpCol < board[tmpRow].length) {
            if(board[tmpRow][tmpCol] != POSITION_FREE && board[tmpRow][tmpCol] != POSITION_BUSY) {
                return false;
            }
            tmpRow += 1;
            tmpCol += 1;
        }
        // checks antidiagonal first half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow < board.length && tmpCol >= 0) {
            if(board[tmpRow][tmpCol] != POSITION_FREE && board[tmpRow][tmpCol] != POSITION_BUSY) {
                return false;
            }
            tmpRow += 1;
            tmpCol -= 1;
        }
        // checks antidiagonal second half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow >= 0 && tmpCol < board[tmpRow].length) {
            if(board[tmpRow][tmpCol] != POSITION_FREE && board[tmpRow][tmpCol] != POSITION_BUSY) {
                return false;
            }
            tmpRow -= 1;
            tmpCol += 1;
        }
        return true;
    }
    /**
     * Sets all diagonals from the provided position under attack.
     * 
     * @param   row     row of the position
     * @param   col     col of the position
     */
    public void setDiagonalsUnderAttack(int row, int col) {
        int tmpRow;
        int tmpCol;
        // sets main diagonal first half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow >=0 && tmpCol >= 0) {
            board[tmpRow][tmpCol] = POSITION_BUSY;
            tmpRow -= 1;
            tmpCol -= 1;
        }
        // sets main diagonal second half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow < board.length && tmpCol < board[tmpRow].length) {
            board[tmpRow][tmpCol] = POSITION_BUSY;
            tmpRow += 1;
            tmpCol += 1;
        }
        // sets antidiagonal first half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow < board.length && tmpCol >= 0) {
            board[tmpRow][tmpCol] = POSITION_BUSY;
            tmpRow += 1;
            tmpCol -= 1;
        }
        // sets antidiagonal second half
        tmpRow = row;
        tmpCol = col;
        while(tmpRow >= 0 && tmpCol < board[tmpRow].length) {
            board[tmpRow][tmpCol] = POSITION_BUSY;
            tmpRow -= 1;
            tmpCol += 1;
        }
    }
    /**
     * Starting from the current position finds the next free position.
     * 
     * @return          <code>true</code> if there is a next position available
     *                  <code>false</code> otherwise
     */
    public boolean next() {
        boolean isNext = false;
        int tempCol = currentCol + 1;
        int tempRow = currentRow;
        if(tempCol >= board[currentRow].length) {
            tempCol = 0;
            tempRow++;
        }
        outer:
        for(int i = tempRow; i < board.length; i++) {
            for(int j = tempCol; j < board[i].length; j++) {
                if(board[i][j] == POSITION_FREE) {
                    currentRow = i;
                    currentCol = j;
                    isNext = true;
                    break outer;
                }
            }
            tempCol = 0;
        }
        return isNext;
    }
    /**
     * Returns a string representation of a chess board with pieces.
     */
    @Override
    public String toString() {
        String s = "";
        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board[i].length; j++) {
                if(board[i][j] == Piece.BISHOP.value()) {
                    s += "B";
                } else if(board[i][j] == Piece.KING.value()) {
                    s += "K";
                } else if(board[i][j] == Piece.KNIGHT.value()) {
                    s += "N";
                } else if(board[i][j] == Piece.QUEEN.value()) {
                    s += "Q";
                } else if(board[i][j] == Piece.ROOK.value()) {
                    s += "R";
                } else {
                    s += "_";
                }
            }
            s += "\n";
        }
        return s;
    }
}
