package board.strategies;

import board.ChessBoard;
import board.Piece;

/**
 * Attack strategy implementation for a Bishop.
 * A bishop attacks all diagonals.
 * 
 * @author Petar Chakarov
 *
 */
public class BishopStrategy implements AttackStrategy {
    /**
     * Places a bishop on the provided board.
     * Marks all positions on all diagonals as under attack.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the piece is placed successfully
     *          <code>false</code> otherwise
     */
    public boolean place(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(check(board) == false) {
            return false;
        }
        board.setDiagonalsUnderAttack(row, col);
        board.placePieceOnPosition(Piece.BISHOP.value());
        return true;
    }
    /**
     * Checks if all positions on all diagonals are free.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the positions are free
     *          <code>false</code> otherwise
     */
    private boolean check(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(board.areDiagonalsFree(row, col)) {
           return true;
        }
        return false;
    }
}
