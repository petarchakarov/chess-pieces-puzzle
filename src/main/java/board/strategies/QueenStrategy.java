package board.strategies;

import board.ChessBoard;
import board.Piece;

/*
 * Attack strategy for a Queen.
 * A queen attacks its current row, column and on all diagonals from its current position.
 */
/**
 * Attack strategy implementation for a Queen.
 * A queen attacks its current row, column and on all diagonals from its current position.
 * 
 * @author Petar Chakarov
 *
 */
public class QueenStrategy implements AttackStrategy {
    /**
     * Places a queen on the provided board.
     * Marks all positions on the current row, current column and all diagonals as under attack.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the piece is placed successfully
     *          <code>false</code> otherwise
     */
    public boolean place(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(check(board) == false) {
            return false;
        }
        board.setRowUnderAttack(row);
        board.setColUnderAttack(col);
        board.setDiagonalsUnderAttack(row, col);
        board.placePieceOnPosition(Piece.QUEEN.value());
        return true;
    }
    /**
     * Checks if all positions on the current row, current column and all diagonals are free.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the positions are free
     *          <code>false</code> otherwise
     */
    private boolean check(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(board.isRowFree(row) && board.isColFree(col) && board.areDiagonalsFree(row, col)) {
           return true;
        }
        return false;
    }
}