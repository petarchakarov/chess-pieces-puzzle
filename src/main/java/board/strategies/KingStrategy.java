package board.strategies;

import board.ChessBoard;
import board.Piece;

/**
 * Attack strategy implementation for a King.
 * A king attacks all neighboring positions.
 * 
 * @author Petar Chakarov
 *
 */
public class KingStrategy implements AttackStrategy {
    /**
     * Places a king on the provided board.
     * Marks all positions neighboring positions as under attack.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the piece is placed successfully
     *          <code>false</code> otherwise
     */
    public boolean place(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(check(board) == false) {
            return false;
        }
        board.placePieceOnPosition(Piece.KING.value());
        board.setPositionUnderAttack(row, col + 1);
        board.setPositionUnderAttack(row, col - 1);
        board.setPositionUnderAttack(row + 1, col);
        board.setPositionUnderAttack(row + 1, col + 1);
        board.setPositionUnderAttack(row + 1, col - 1);
        board.setPositionUnderAttack(row - 1, col);
        board.setPositionUnderAttack(row - 1, col + 1);
        board.setPositionUnderAttack(row - 1, col - 1);
        return true;
    }
    /**
     * Checks if all neighboring positions are free.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the positions are free
     *          <code>false</code> otherwise
     */
    private boolean check(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(    //current row
               board.isPositionFree(row, col + 1) && board.isPositionFree(row, col - 1) &&
               //next row
               board.isPositionFree(row + 1, col) && board.isPositionFree(row + 1, col + 1) && board.isPositionFree(row + 1, col - 1) &&
               //previous row
               board.isPositionFree(row - 1, col) && board.isPositionFree(row - 1, col + 1) && board.isPositionFree(row - 1, col - 1)) {
           return true;
        }
        return false;
    }
}