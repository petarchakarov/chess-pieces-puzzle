package board.strategies;

import board.ChessBoard;
import board.Piece;

/*
 * Attack strategy for a Rook.
 * A rook can attack its current row and column.
 */
/**
 * Attack strategy implementation for a Rook.
 * A rook attacks its current row and column.
 * 
 * @author Petar Chakarov
 *
 */
public class RookStrategy implements AttackStrategy {
    /**
     * Places a rook on the provided board.
     * Marks all positions on the current row and current column as under attack.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the piece is placed successfully
     *          <code>false</code> otherwise
     */
    public boolean place(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(check(board) == false) {
            return false;
        }
        board.setRowUnderAttack(row);
        board.setColUnderAttack(col);
        board.placePieceOnPosition(Piece.ROOK.value());
        return true;
    }
    /**
     * Checks if all positions on the current row and current column are free.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the positions are free
     *          <code>false</code> otherwise
     */
    private boolean check(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(board.isRowFree(row) && board.isColFree(col)) {
           return true;
        }
        return false;
    }
}