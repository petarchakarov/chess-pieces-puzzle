package board.strategies;

import board.ChessBoard;
import board.Piece;

/**
 * Attack strategy implementation for a Knight.
 * A knight attacks on the letter L pattern.
 * 
 * @author Petar Chakarov
 *
 */
public class KnightStrategy implements AttackStrategy {
    /**
     * Places a knight on the provided board.
     * Marks all positions forming the L pattern as under attack.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the piece is placed successfully
     *          <code>false</code> otherwise
     */
    public boolean place(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(check(board) == false) {
            return false;
        }
        board.placePieceOnPosition(Piece.KNIGHT.value());
        board.setPositionUnderAttack(row - 1, col - 2);
        board.setPositionUnderAttack(row + 1, col - 2);
        board.setPositionUnderAttack(row + 2, col - 1);
        board.setPositionUnderAttack(row + 2, col + 1);
        board.setPositionUnderAttack(row - 1, col + 2);
        board.setPositionUnderAttack(row + 1, col + 2);
        board.setPositionUnderAttack(row - 2, col - 1);
        board.setPositionUnderAttack(row - 2, col + 1);
        return true;
    }
    /**
     * Checks if all positions forming the L pattern are free.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the positions are free
     *          <code>false</code> otherwise
     */
    private boolean check(ChessBoard board) {
        int row = board.getCurrentRow();
        int col = board.getCurrentCol();
        if(board.isPositionFree(row - 1, col - 2) && board.isPositionFree(row + 1, col - 2) &&
               board.isPositionFree(row + 2, col - 1) && board.isPositionFree(row + 2, col + 1) &&
               board.isPositionFree(row - 1, col + 2) && board.isPositionFree(row + 1, col + 2) &&
               board.isPositionFree(row - 2, col - 1) && board.isPositionFree(row - 2, col + 1)) {
           return true;
        }
        return false;
    }
}