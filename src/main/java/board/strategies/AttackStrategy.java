package board.strategies;

import board.ChessBoard;

/**
 * Interface for the chess piece attack Strategy pattern.
 * All chess pieces implement this interface to provide strategies for placing a piece on the chess board.
 * 
 * @author Petar Chakarov
 *
 */
public interface AttackStrategy {
    /**
     * Places a chess piece on the provided board.
     * 
     * @param board board to place the piece on
     * @return  <code>true</code> if the piece is placed successfully
     *          <code>false</code> otherwise
     */
    boolean place(ChessBoard board);
}
