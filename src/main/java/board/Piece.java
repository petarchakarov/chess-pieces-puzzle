package board;

/**
 * Enum representing chess pieces.
 * 
 * @author Petar Chakarov
 *
 */
public enum Piece {
	BISHOP(2), KING(3), KNIGHT(4), QUEEN(5), ROOK(6);
	
	private int value;
	Piece(int value) {
		this.value = value;
	}
	/**
	 * 
	 * @return 2 for Bishop, 3 for King, 4 for Knight, 5 for Queen and 6 for Rook
	 */
	public int value() {
		return this.value;
	}
}
